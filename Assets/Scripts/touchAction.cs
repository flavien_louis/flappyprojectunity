﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchAction : MonoBehaviour {
	private GameObject myBird;
	AudioSource touchSound;

	// Use this for initialization
	void Start () {
		this.myBird = GameObject.Find("bird");
		touchSound = GetComponent<AudioSource>();
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown(0)){
			this.myBird.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 10);
			this.playSoundBump();
		}
	}

	public void playSoundBump(){
		touchSound.Play(0);
	}
}
