﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonPlay : MonoBehaviour {
	 void Start()
    {
        // Only specifying the sceneName or sceneBuildIndex will load the Scene with the Single mode
    }

	public void onClick(){
		Debug.Log ("Clicked");
        SceneManager.LoadScene("scene3-Game", LoadSceneMode.Single);
	}
}
