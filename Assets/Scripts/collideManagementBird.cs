﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class collideManagementBird : MonoBehaviour {
	public GameObject bird;
	private bool hasLoose;
    private Vector3 leftBottomCameraBorder;
	

	// Use this for initialization
	void Start () {
		this.bird = GameObject.Find("bird");
		SingletonScore.Instance.reset();
		this.hasLoose = false;
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
		if (this.hasLoose && this.bird.transform.position.y < leftBottomCameraBorder.y){
			//Si le tag hasLoose n'a pas encore été mis et que le score n'a pas été enregistré
			//j'attends de passer le bas de l'écran pour changer de scène
			SceneManager.LoadScene("scene4-Loose", LoadSceneMode.Single);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log(collider.gameObject.tag);
		if(collider.gameObject.tag == "pipe"){
			Debug.Log("Loose");
			Destroy(bird.GetComponent<touchAction>());
			bird.GetComponent<Rigidbody2D>().angularVelocity = -500;
			this.hasLoose = true;
			SingletonScore.Instance.save();
		}else if(collider.gameObject.tag == "score" && !this.hasLoose){
			SingletonScore.Instance.scoreUp(1);
			Debug.Log(SingletonScore.Instance.score);
		}
	}
}
