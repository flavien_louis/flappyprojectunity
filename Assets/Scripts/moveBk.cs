﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour {
	public float positionRestartX;
	public Vector2 movement;
    private Vector3 leftBottomCameraBorder;
	public Vector2 siz;

	void Update()
	{
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		GetComponent<Rigidbody2D>().velocity = movement;
			siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
			siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
			// If the backgound exits the screen
			// Set the X position with the original backGround3 X position
		if (transform.position.x < leftBottomCameraBorder.x - (siz.x / 2))
		{
			transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		}
	}
}
