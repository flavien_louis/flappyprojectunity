﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour {
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;

	public Vector2 movement;
	public GameObject pipe1Up;
	public GameObject pipe1Down;
	public GameObject scoreUp;
	private Transform pipe1UpOriginalTransform;
	private Transform pipe1DownOriginalTransform;

	private Vector2 siz;

	// Use this for initialization
	void Start () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));

		this.pipe1UpOriginalTransform = pipe1Up.transform;
		this.pipe1DownOriginalTransform = pipe1Down.transform;
	}
	
	// Update is called once per frame
	void Update()
	{
		pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
		pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
		scoreUp.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du collider de score
		siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x; // Récuperation de la taille d’un pipe
		siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y; // Suffisant car ils ont la même taille
		// Le pipe est sorti de l’écran ? Si oui appel de la méthode moveTORightPipe
		if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
	}
	void moveToRightPipe(){
		float randomY = Random.Range (1,4) - 2; // Tirage aléatoire d’un décalage en Y
		float posX = rightBottomCameraBorder.x + (siz.x / 2); // Calcul du X du bord droite de l’écran
		// Calcul du nouvel Y en reprenant la position Y d’origine du pipe, ici le downPipe1
		float posY = pipe1UpOriginalTransform.position.y + randomY;
		// Création du vector3 contenant la nouvelle position
		Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		// Affectation de ceVe nouvelle position au transform du gameObject
		pipe1Up.transform.position = tmpPos;
		// Idem pour le second pipe
		posY = pipe1DownOriginalTransform.position.y + randomY;
		tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
		pipe1Down.transform.position = tmpPos;

		//repositionnement du score collider
		Vector3 scorePos =  new Vector3 (posX,scoreUp.transform.position.y, scoreUp.transform.position.z);
		scoreUp.transform.position = scorePos;
	}
}
