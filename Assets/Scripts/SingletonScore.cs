﻿using UnityEngine;
using UnityEngine.UI;

public class SingletonScore : Singleton<SingletonScore>
{
    public int score = 0;
    // (Optional) Prevent non-singleton constructor use.
    //protected SingletonScore() { }

    void Start () {
        Debug.Log("Start");        
    }
    public void scoreUp(int scoreToAdd){
        this.score = this.score + scoreToAdd;
        GameObject.FindWithTag("ScoreText").GetComponent<Text>().text = "Score : " + this.score;
    }

    public void reset(){
        this.score = 0;
    }

    public void save(){
        Debug.Log("Score to save");
    }

    public void getScore(){
        return this.score;
    }
}